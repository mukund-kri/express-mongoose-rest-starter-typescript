import { Gulpclass, Task, SequenceTask } from "gulpclass/Decorators";
import * as gulp  from 'gulp';
import * as ts from 'gulp-typescript';
import * as nodemon from 'gulp-nodemon';
import * as mocha from 'gulp-mocha';

let del = require('del');
const Cache = require('gulp-file-cache');

const SERVER_CODE = '/server/**/*.ts';
const BUILD_DIR = './build';
const TEST_DIR = './tests/server';

let tsProject = ts.createProject('tsconfig.json');
let cache = new Cache();
let mochaOps = {
  compilers: [
    'ts:ts-node/register'
  ]
};


@Gulpclass()
export class Gulpfile {

  /* DEVELOPMENT RELATED TASKS ---------------------------------------------- */

  // Compile typescript to es5
  @Task('compile:ts')
  compileTs() {
    let stream = tsProject.src()
      .pipe(cache.filter())
      .pipe(ts(tsProject))
      .pipe(cache.cache())
      .pipe(gulp.dest(BUILD_DIR));
    return stream;
  };

  @Task('copy:templates')
  copyTemplates() {
    return gulp.src('./**/*.pug')
      .pipe(gulp.dest('./build'))
  }

  // Use nodemon to monitor and restart the code
  @Task("watch:ts", ["compile:ts"])
  watchTs() {
    let stream = nodemon({
      script: `${BUILD_DIR}/server/develop.js`,
      watch: ['server'],
      ext: 'ts',
      tasks: ['compile:ts']
    });
    return stream;
  }

  @Task('develop', ['watch:ts'])
  develop() {}

  @Task()
  default() {
    return ['develop']
  }

  /* TESTING RELATED TASKS -------------------------------------------------- */
  @Task('test:models')
  testModels() {
    return gulp.src(`${TEST_DIR}/models/*.spec.ts`)
      .pipe(mocha(mochaOps));
  }

  @Task('test:routes', ['compile:ts', 'copy:templates'])
  testRoutes() {
    return gulp.src(`${TEST_DIR}/routes/*.spec.ts`)
      .pipe(mocha(mochaOps));
  }

  @Task('test:api', ['compile:ts'])
  testApi() {
    return gulp.src(`${TEST_DIR}/api/*.spec.ts`)
      .pipe(mocha(mochaOps));
  }

  @SequenceTask('test' )
  test() {
    return ['test:models', 'test:routes', 'test:api'];
  }
}
