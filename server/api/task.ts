import { Router, Request, Response } from 'express';
var createError = require('http-errors');

import Task from '../models/task';


export const taskApiRoutes = Router();
// const wrap = fn => (req, res, next) => fn(req, res, next).catch(next);


taskApiRoutes.get('/', async (req: Request, res: Response, next:any) => {
  let task = await Task.find({});
  res.json(task);
});


taskApiRoutes.get('/:id', async(req: Request, res: Response, next: any) => {

  let docId = req.params.id;
  let task: any = null;
  try {
    task = await Task.findById(docId);
    if(task) res.json(task);
    else res.status(404).json({ error: 'Task not found'});
  } catch (err) {
    res.status(404).json({ error: 'Task not found'});
  }

});


taskApiRoutes.post('/', async(req: Request, res: Response, next: any) => {
  let task = new Task({
    text: req.body.text
  });
  await task.save();
  res.json({ success: 'Task created', task: task });
});


taskApiRoutes.delete('/:id', async(req: Request, res: Response, next: any) => {
  let docId = req.params.id;
  let task = await Task.findByIdAndRemove(docId);

  if(task) res.json({ success: 'Task deleted'});
  else res.status(404).json({ error: 'Task not found'});
});


taskApiRoutes.put('/:id', async(req: Request, res: Response, next: any) => {
  let docId = req.params.id;

  let updateQuery: any = { $set: {} };
  for(let prop in req.body) {
    updateQuery.$set[prop] = req.body[prop];
  }
  let task = await Task.findByIdAndUpdate(docId, updateQuery);

  if(task) res.json({ success: 'Task updated'});
  else res.status(404).json({ error: 'Task not found'});
});

// Error handling for this api
taskApiRoutes.use(function(err:any, req:any, res:any, next:any) {
  console.log("ERRRRRRRRRRRRRRRRRRRRRr -------------->")
  if(err instanceof createError.NotFound) {
    // Task not found
    res.status(err.statusCode).json({ error: err.message });
  } else {
    // We don't want the end user to see our exception.
    console.log(err);
    res.status(500).json({ error: 'Internal error' });
  }
});



export default taskApiRoutes;
