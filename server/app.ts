import * as express from 'express';
import * as path from 'path';
import * as bodyParser from 'body-parser';

import {expressConfig} from './config/server.config';

// Instantiate an express instance
export const app = express();

for(var property in expressConfig) {
  let value: string = (<any>expressConfig)[property]
  app.set(property, value);
}

// for parsing application/json
app.use(bodyParser.json());
// for parsing application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// register routes
import homeRoutes from './routes/home';
app.use('/', homeRoutes);

import { taskApiRoutes } from './api/task';
app.use('/api/task', taskApiRoutes);
