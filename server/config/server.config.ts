
import {resolve} from 'path';


// Configure mongoose to use the native node promises
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;


// Mongo related configurations
export const mongoConfig  = {
  dev: {
    url: 'mongodb://localhost/devdb'
  },
  test: {
    url: 'mongodb://localhost/testdb'
  }
}

// ExpressJS related configurations
export const expressConfig = {
  'views' : resolve(__dirname, '../', 'views'),
  'view engine' : 'pug'
}
