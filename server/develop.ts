import { connect } from 'mongoose';

import { app } from './app';
import { mongoConfig } from './config/server.config';


connect(mongoConfig.dev.url);


app.listen(4000, (err:any) => console.log("Listening on port 4000"));
