import { Document, Schema, model, Types } from 'mongoose';


interface ITask extends Document {
  text: string;
  createTime: Date;
  status: boolean;
}

export interface ITaskModel extends ITask {}

let taskSchema = new Schema({
  text:       { type: String, required: true },
  createTime: { type: String, default: Date.now },
  status:     { type: String, default: false }
});

export const Task = model<ITaskModel>('Task', taskSchema);

export default Task;
