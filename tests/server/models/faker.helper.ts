import { lorem } from 'faker';

export function genTaskData(): any {
  return {
    text: lorem.sentence(6)
  }
};
