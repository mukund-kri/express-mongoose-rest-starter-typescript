import { expect } from 'chai';
import * as mongoose from 'mongoose';

import { Task, ITaskModel } from '../../../server/models/task';
import { mongoConfig } from '../../../server/config/server.config';

import { genTaskData } from './faker.helper';


const clearDB = require('mocha-mongoose')(mongoConfig.test.url, {noClear: true});


describe('#TASK model', () => {

  before(async function() {
    if(mongoose.connection.db) return;
    await mongoose.connect(mongoConfig.test.url);
  });


  after(function(done) {
    clearDB(done);
  });


  it('#Can be instantiated and saved', async function() {
    let task = new Task({ text: 'New task' });
    await task.save()
    expect(task._id).to.be.ok;
  });


  it('#Can be creaed', async function() {
    let fakeTaskData = genTaskData();
    let task  = await Task.create(fakeTaskData);

    expect(task).property('text', fakeTaskData.text);
  });


  it('#can be listed', async function() {
    let tasks = await Task.find({});
    expect(tasks.length).to.equal(2);
  });


  // TODO: You know the drill, add more tests here ...
});
