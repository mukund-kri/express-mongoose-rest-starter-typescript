import { expect } from 'chai';
import  supertest  = require('supertest-as-promised');

import { app } from '../../../server/app';


describe('#Home routes', () => {
  it('#Home page should render correctly', async function() {
    let response = await supertest(app)
      .get('/')
      .expect('content-type', /text/)
      .expect(200)

    expect(response.text).to.match(/Your\ App/)
  });
});
